# Servant for Noobies (by Noobies) - The notes

## Goals

* REST API with some sort of CRUD
* Logging
* Some Transactional, Queryable persistence
* Some external config

## Instructions

1. Create a project via
         stack new s4n servant
         
1. Show off what's already there
1. `stack build` and prepare to talk about yourself for 15 Minutes



